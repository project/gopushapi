<?php

/**
 * Implements hook_permission().
 */
function gopushapi_client_permission() {
  return array(
    'administer gopushapi client' => array(
      'title' => t('Administer GoPush API Client')
    ),
  );
}

/**
 * Implements hook_menu().
 */
function gopushapi_client_menu() {
  $items = array();

  $items['admin/config/services/gopushapi_client'] = array(
    'title' => 'GoPush API Client',
    'access arguments' => array('administer gopushapi service'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('gopushapi_client_admin_form'),
  );

  return $items;
}

/**
 * Menu callback for 'admin/config/services/gopushapi_client'.
 */
function gopushapi_client_admin_form() {
  $form = array();

  $form['gopushapi_client_service'] = array(
    '#title' => t('Service path'),
    '#type' => 'textfield',
    '#default_value' => variable_get('gopushapi_client_service'),
    '#description' => t('Do not include the protocol!<br />e.g.: example.com, not http://example.com.')
  );

  return system_settings_form($form);
}

/**
 * Registers a client.
 *
 * @param string $center
 *   Name of the notification center.
 * @param string $onmessagereceive
 *   Javascript callback name, which will be called when a message is received.
 *   Namespaced names are also accepted.
 * @param string $onconnect
 *   Javascript callback name, which will be called when a connection is
 *   established.
 *   Namespaced names are also accepted.
 * @param string $ondisconnect
 *   Javascript callback name, which will be called when a connection is
 *   closed.
 *   Namespaced names are also accepted.
 * @param bool $secure
 *   Set this TRUE to use HTTPS and WSS protocol instead of HTTP and WS.
 * @param string $path
 *   The address of the server. This is fetched from the gopushapi_client_service
 *   variable by default.
 *   This must not contain the protocol (http://, https://, ws://, wss://).
 */
function gopushapi_client_addlistener($center, $onmessagereceive, $onconnect = '', $ondisconnect = '', $secure = FALSE, $path = NULL) {
  drupal_add_js(drupal_get_path('module', 'gopushapi_client') . '/gopushapi_client.js');
  drupal_add_js(array(
    'gopushapi_clients' => array(
      array(
        'path' => $path ?: variable_get('gopushapi_client_service'),
        'center' => $center,
        'onmessagereceive' => $onmessagereceive,
        'onconnect' => $onconnect,
        'ondisconnect' => $ondisconnect,
        'secure' => $secure,
      ),
    ),
  ), 'setting');
}
